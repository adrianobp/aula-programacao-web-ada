package tech.ada.aula.web.model.dto;

import java.time.LocalDate;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Cliente {
	private String nome;
	private String cpf;
	private LocalDate dataNascimento;
}
