package tech.ada.aula.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import tech.ada.aula.web.model.dto.Cliente;

@RestController
@RequestMapping("/clientes")
@Slf4j
public class ClienteController {

	private List<Cliente> clientes = new ArrayList<>();
	
	@GetMapping
	public ResponseEntity<List<Cliente>> buscarTodos() {
		return ResponseEntity.ok(clientes);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> buscarUm(@PathVariable("id") Integer id) {
		try {
			
			if(clientes.size()>=id) {
				return ResponseEntity.ok(clientes.get(id));
			}
			
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
			
		}catch(Exception ex) {
			log.error(ex.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@PostMapping
	public ResponseEntity<Cliente> criar(@RequestBody Cliente cliente) {
		try {
			clientes.add(cliente);
			
			return ResponseEntity
					.status(HttpStatus.CREATED)
					.contentType(MediaType.APPLICATION_JSON)
					.body(cliente);
			
		}catch(Exception ex) {
			log.error(ex.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Cliente> editar(@PathVariable("id") Integer id,
						@RequestBody Cliente cliente) {
		try {
			if(clientes.size()>=id) {
				clientes.set(id, cliente);
				return ResponseEntity
						.status(HttpStatus.OK)
						.contentType(MediaType.APPLICATION_JSON)
						.body(cliente);
			}
			
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
			
		}catch(Exception ex) {
			log.error(ex.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> remover(@PathVariable("id") Integer id) {
		try {
			if(clientes.size()>=id) {
				clientes.remove(id.intValue());
				return ResponseEntity
						.status(HttpStatus.OK)
						.build();
			}
			
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}catch(Exception ex) {
			log.error(ex.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
}
